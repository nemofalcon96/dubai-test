import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BaseModel } from './core/models/base.model';
import { BaseService } from './core/services/base.service';
import * as moment from 'moment'
import { PlanModel } from './core/models/plan.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  categories: any[] = [];
  locations: any[] = [];
  languages: any[] = [];
  socialNetworks: any[] = [];
  counter: number = 0;
  form!: FormGroup;
  preview: string = '';
  images: string[] = [];
  campaign: BaseModel = new BaseModel();
  myFiles: any[] = [];
  plan: PlanModel = new PlanModel();
  is_saved: boolean = false;
  budget?: number;
  constructor(
    private toaster: ToastrService,
    private baseService: BaseService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getAllCategories();
    this.getAllLanguages();
    this.getAllSocialNetworks();
    this.getLocation();
    this.form = this.formBuilder.group({
      location_id: ['', [Validators.required]],
      language_id: ['', [Validators.required]],
      category_id: ['', [Validators.required]],
      social_network_id: ['', [Validators.required]],
      plan_id: ['', [Validators.required]],
      start_date: [''],
      end_date: [''],
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      delivery_type: ['', [Validators.required]],
      number_freelancer: ['', [Validators.required]],
      budget: ['', [Validators.required]],
      is_saved: ['', [Validators.required]],
      urls: ['', [Validators.required]],
      giveaway: ['', [Validators.required]],
      date: ['', [Validators.required]],
    });
  }

  getAllCategories() {
    this.baseService.getFilterByStatus(2).subscribe(res => {
      this.categories = res
    })

  }
  changed(value: any) {
    this.budget = value?.target.value;
    this.baseService.getPlan(value?.target.value).subscribe(res => {
      this.plan = res;
    })
  }

  getAllSocialNetworks() {
    this.baseService.getFilterByStatus(6).subscribe(res => {
      this.socialNetworks = res;
    })
  }

  getAllLanguages() {
    this.baseService.getFilterByStatus(1).subscribe(res => {
      this.languages = res;
    })
  }

  getLocation() {
    this.baseService.getCounries().subscribe(res => {
      this.locations = res;
    })
  }

  public hasError = (controlName: string, errorName: string) => {
    const control = this.form.controls[controlName];
    return control ? control.hasError(errorName) && this.form.controls[controlName].touched : false;
  }

  uploadFile(event: any): void {
    for (var i = 0; i < event.target.files.length; i++) {
      const reader = new FileReader();
      this.myFiles.push(event.target.files[i]);
      reader.onload = (event: any) => {
        this.images.push(event.target.result);
      };
      reader.readAsDataURL(event.target.files[i]);
    }
    const FILE = (event.target as any).files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    };
    reader.readAsDataURL(FILE);
  }

  filter(index: number) {
    this.myFiles = this.myFiles.filter((x, i) => i != index);
    this.images = [];
    for (var i = 0; i < this.myFiles.length; i++) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.images.push(event.target.result);
      };
      reader.readAsDataURL(this.myFiles[i]);
    }
  }

  public increment(): void {
    this.counter += 1;
  }

  public decrement(): void {
    if (this.counter > 0) {
      this.counter -= 1;
    }
  }

  onSubmit(): void {
    this.campaign = this.createCampaign()
    this.baseService.createCampaign(this.campaign).subscribe(res => {
      this.AddImagesCampaign(res?.campaign_id!)
      this.toaster.success('Campaign succesfully add')
    })
  }

  createCampaign() {
    return {
      plan_id: this.plan.plan_id,
      budget: this.budget,
      category_id: this.form.get('category_id')?.value,
      delivery_type: this.form.get('delivery_type')?.value,
      location_id: this.form.get('location_id')?.value,
      language_id: this.form.get('language_id')?.value,
      description: this.form.get('description')?.value,
      number_freelancer: this.counter,
      urls: [this.form.get('urls')?.value],
      title: this.form.get('title')?.value,
      giveaway: this.form.get('giveaway')?.value,
      social_network_id: this.form.get('social_network_id')?.value,
      start_date: moment(this.form.get('date')?.value[0]).format('YYYY-MM-DD'),
      end_date: moment(this.form.get('date')?.value[1]).format('YYYY-MM-DD'),
      is_saved: this.is_saved
    };
  }
  AddImagesCampaign(id: number): void {
    const fd = new FormData();
    for (var i = 0; i < this.myFiles.length; i++) {
      fd.append("file", this.myFiles[i]);
    }
    this.baseService.createCampaignImages(id, fd).subscribe(res => {
    })
  }

  saved(): void {
    if (this.is_saved) {
      this.is_saved = false;
    } else {
      this.is_saved = true;
    }
  }
}
