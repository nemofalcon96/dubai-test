import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseModel } from '../models/base.model';
import { CountryModel } from '../models/country.model';
import { PlanModel } from '../models/plan.model';
@Injectable({
  providedIn: 'root'
})
export class BaseService {
  baseUrl = `${environment.apiUrl}`;
  constructor(public http: HttpClient) { }

  getFilterByStatus(status: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}common/entity?type=${status}`, { responseType: 'json' });
  }

  createCampaign(model: BaseModel): Observable<BaseModel> {
    return this.http.post<BaseModel>(`${this.baseUrl}campaign/add/`, model, { responseType: 'json' });
  }

  getCounries(): Observable<CountryModel[]> {
    return this.http.get<CountryModel[]>(`${this.baseUrl}common/country`, { responseType: 'json' });
  }

  createCampaignImages(id: number, file: FormData): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}campaign/${id}/upload/media/`, file, { responseType: 'json' });
  }

  getPlan(price: number): Observable<PlanModel> {
    return this.http.get<PlanModel>(`${this.baseUrl}campaign/plan?price=${price}`, { responseType: 'json' });
  }

}
