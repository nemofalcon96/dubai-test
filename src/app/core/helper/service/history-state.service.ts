import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HistoryStateService {

  constructor() {
  }

  public subjectCategory = new Subject<any>();
  public subjectSubCategory = new Subject<any>();

  SET(value: any): void {
    this.subjectCategory.next(value);
  }

  GET(): Observable<any> {
    return this.subjectCategory.asObservable();
  }

  SETSUB(value: any): void {
    this.subjectSubCategory.next(value);
  }

  GETSUB(): Observable<any> {
    return this.subjectSubCategory.asObservable();
  }

}



