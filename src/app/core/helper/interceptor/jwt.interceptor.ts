import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from "@angular/core"
import { Observable, of } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
@Injectable()
export class JwtHttpInterceptor implements HttpInterceptor {
    constructor(public toasterService: ToastrService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
            setHeaders: {
                Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE4ODMwNjY1LCJqdGkiOiIwZTkxN2YxYjdiZTI0ZjA2ODE2OTNkOTNlYTI2Mjc0NSIsInVzZXJfaWQiOjE0NywicGVybWlzc2lvbnMiOlsiYWNjb3VudHMuYWRkX2NhcmQiLCJhY2NvdW50cy52ZXJpZnlfbWFpbCIsImFjY291bnRzLmNvbXBsZXRlX3Byb2ZpbGUiXSwidXNlcl9yb2xlIjoyfQ.2tRdrKpzG39raLEOia3hVfaOILYp82zAPZHhM3w_aeY`,
            }
        })

        return next.handle(req).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse) {
                    if (evt.body.message) {
                        this.toasterService.success(evt.body.message);
                    }
                }
            }),
            catchError((err: any) => {
                if (err instanceof HttpErrorResponse) {
                    try {
                        this.toasterService.error(err.error.message);
                    } catch (e) {
                        this.toasterService.error('An error occurred', '');
                    }
                }
                return of(err);
            }));
    }

}