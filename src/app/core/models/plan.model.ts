export class PlanModel {
  is_empty?:boolean;
  plan_id?:number;
  influencer_min?:number;
  influencer_max?:number;
  coverage_min?:number;
  coverage_max?:number;
}
