export class BaseModel {
    id?:number;
    location_id?:number[];
    language_id?:number[];
    category_id?:number[];
    social_network_id?:number[];
    plan_id?:number;
    start_date?: string;
    end_date?:string;
    title?: string;
    description?: string;
    delivery_type?: string;
    number_freelancer?: number;
    budget?: number;
    is_saved?:boolean;
    urls?: string[];
    giveaway?: string;
    file?: File;
    campaign_id?:number
}
